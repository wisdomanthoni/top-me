<?php

/*
 * This file is part of the Laravel Paystack package.
 *
 * (c) Prosper Otemuyiwa <prosperotemuyiwa@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /**
     * Public Key From Paystack Dashboard
     *
     */
    // 'publicKey' => getenv('PAYSTACK_PUBLIC_KEY'),

      'publicKey' => 'pk_test_ba1b970c33368bfff64d73205bc95c3be5bc6bf3',

    /**
     * Secret Key From Paystack Dashboard
     *
     */
     //'secretKey' => getenv('PAYSTACK_SECRET_KEY'),
      'secretKey' => 'sk_test_99d9bcdcd9bb10e267183522347fa21081785aba',

    /**
     * Paystack Payment URL
     *
     */
   // 'paymentUrl' => getenv('PAYSTACK_PAYMENT_URL'),

      'paymentUrl' => 'https://api.paystack.co',

    /**
     * Optional email address of the merchant
     *
     */
    //'merchantEmail' => getenv('MERCHANT_EMAIL'),

      'merchantEmail' => 'wisdomanthoni@maildrop.cc',

];
