<?php

use Illuminate\Database\Seeder;
use App\User;

class UsernameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach ($users as $user) {
        	$name = explode(' ', strtolower($user->name));
        	$name = implode('-', $name);
        	$username = $name . mt_rand(1, 99999);
        	$user->username = $username;
        	$user->save();
        }
    }
}
