<?php

use Illuminate\Database\Seeder;
use App\Currency;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currency = new Currency;
        $currency->code = 'NGN';
        $currency->name = 'Naira';
        $currency->symbol = '₦';
        $currency->save();

        $currency = new Currency;
        $currency->code = 'USD';
        $currency->name = 'US Dollar';
        $currency->symbol = '$';
        $currency->save();
    }
}
