<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Photo;
class PhotosToHttps extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach ($users as $user) {
        	if ($user->picture != null ) {
        		$user->picture = 'https'.substr($user->picture, 4);
        		$user->save();
        	}
        	
        }

        $photos = Photo::all();
        foreach ($photos as $photo) {
        	$photo->url = 'https'. substr($photo->url, 4);
        	$photo->save();
        }
    }
}
