@extends('layouts.app')
<div class="container center ">
        <div class="quote">
            <p class="quotee">"Anybody can do something about <br> anything, and everyone should try".</p>
            <p class="quoter">Basil Udotai</p>
        </div>
        <div class="box">
            <div class="container d-block">
            <p class="up">Log In
            <hr class="hr">
          </p>

            <div class="form-group ">
            <input class="form-control " type="email" placeholder="Email Address">
            </div>
            <div class="form-group">
            <input class="form-control" type="password" placeholder="Password">
            </div>

            <div class="row">
                <div class="col-md-6 col-md-6">
                    <a href="">Forgot Password?</a>
                </div>
                <div class="col-md-6">
                    Remember me
                </div>
            </div>
            <button type="button" class="btn Next form-control"><a class="next " href="#" >Next</a></button> <br>
                        <span>or</span>     <br>
            <button type="button" class="btn facebook form-control"> <i class="fa fa-facebook-official" aria-hidden="true"></i> <a class="facebook " href="#" >Login with Facebook</a></button>  <br>
            <p class="Remind">Don't have an account? <a class="log" href="">Sign Up </a></p>
          </div>
      </div>
    </div>

    {{--<div class="d-flex d-sm-flex d-md-block d-lg-block d-xl-block my-0 py-0">
       <h6 class="mr-auto mb-0"><a href="">Follow Us</a></h6>

       <div class="horizontal-line my-4 d-none d-sm-none d-md-flex d-lg-flex d-xl-flex"></div>

       <p class="copyright-text mb-0 pr-5"><a href=""><span class="copyright">&copy;</span> <?php echo date('Y') ?> <b>TOPme</b></a></p>
     </div>--}}
@endsection
