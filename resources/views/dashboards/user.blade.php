@extends('layouts.app-dashboard')

@section('content')

@include('chrome.header-dashboard')

<section>

      <div class="nav-color">
        <div class="container container-fluid d-flex justify-content-center">
          <ul class="nav mb-3 nav-color" id="pills-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active navitems" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><img class= "svg1" src="img/dashboard_black_24px.png"> Dashboard</a>
            </li>
            <li class="nav-item">
              <a class="nav-link navitems" id="pills-favouites-tab" data-toggle="pill" href="#pills-favourites" role="tab" aria-controls="pills-favourites" aria-selected="false"><img class= "svg1" src="img/favorites-button.png"> Favorites</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link navitems" id="pills-contributions-tab" data-toggle="pill" href="#pills-contributions" role="tab" aria-controls="pills-contributions" aria-selected="false"><img class= "svg1" src="img/contributions.png"> Contributions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link navitems" id="pills-settings-tab" data-toggle="pill" href="#pills-settings" role="tab" aria-controls="pillssettings" aria-selected="false"><img class= "svg1" src="img/settings_black_24px.png"> Settings</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active py-3" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
          <!-- Body contents -->
          <div class="container container-fluid py-5">

            <div class="d-flex justify-content-between">
              <div class="d-block">
                <h3> Good to have you here.</h3>
                <p><span class="sm-text">Having difficulty with navigation?</span> <span class="click-here"><a href="#">Click Here</a></span></p>
                <hr class="hr-line">
              </div>
              <div class="d-block">
                <h4 class="text-right">Favourite Posts <a href="#"><img class= "svg1" src="img/down-arrow.svg"></a></h4>
              </div>
            </div>

            <div class="">
              @include('includes.user-liked-stories')
            </div>

            {{-- <div class="d-flex justify-content-end">
              <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-end">
                  <li class="page-item"><a class="page-link" href="#" tabindex="-1">Prev.</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">...</a></li>
                  <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>
              </nav>
            </div> --}}

          </div>

        </div>
        <div class="tab-pane fade" id="pills-favourites" role="tabpanel" aria-labelledby="pills-favouites-tab">@include('user.favourites')</div>
        <div class="tab-pane fade" id="pills-contributions" role="tabpanel" aria-labelledby="pills-contributions-tab">@include('user.contributions')</div>
        <div class="tab-pane fade" id="pills-settings" role="tabpanel" aria-labelledby="pills-settings-tab">@include('settings.sia-settings')</div>
      </div>
</section>
@endsection
