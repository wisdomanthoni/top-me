@extends('layouts.app')


@section('content')
<style>
  ::placeholder {
    font-size: 12px;
  }

</style>
<div class="jumbotron jumbotron-fluid d-flex justify-content-center align-items-center"  id="listing-banner">
  <div class="container text-center">
    <p class="f-40 font-weight-bold text-white d-none d-md-block">Our Social Impact Agents (SIAs)</p>
    <p class="f-24 font-weight-bold text-white d-block d-md-none">Our Social Impact Agents (SIAs)</p>
    <div class="d-flex justify-content-center">
      <form class="form-inline" id="searchForm">
        <input class="form-control form-control-lg"  type="text" placeholder="Search SIA's by name" id="form-control-search" name="search" size="30">
        <!-- <i class="fas fa-search"></i> -->
      </form>
      <div class="dropdown px-1">
        <button type="submit" onclick="search()" class="btn filter-btn bg-white">Search</button>
        {{-- <button type="button" class="btn dropdown-toggle filter-btn bg-white" data-toggle="dropdown">
          Filter by
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#">Link 1</a>
          <a class="dropdown-item" href="#">Link 2</a>
          <a class="dropdown-item-text" href="#">Text Link</a>
        </div> --}}
      </div>
    </div>
  </div>
</div>

<section id=" " class="my-5">
  <div class="container container-fluid  ">
    <nav class="px-0 mx-2 border-0 ">
      <div class="nav nav-tabs border-0  font-weight-bold" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active bg-white border-0 rounded-0 text-dark f-18" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Our Social Impact Agents (SIAs)</a>
        <a class="nav-item nav-link bg-white border-0 rounded-0 text-dark f-18" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Trending</a>
        <a class="nav-item nav-link bg-white border-0 rounded-0 text-dark f-18" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Newest</a>
        <a class="nav-item nav-link bg-white border-0 rounded-0 text-dark f-18" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Most Successful</a>
        <a class="nav-item nav-link bg-white border-0 rounded-0 text-dark f-18" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Highly Funded</a>
      </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">@include('includes.all-sias')</div>
      <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
      <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
    </div>
  </div>
</section>
@if(count($sias)>9)
<div class="d-flex justify-content-center mb-5">
  <button type="button" class="btn text-white px-4" style="background-color:#490F83">View Less <span class="fas fa-arrow-up ml-2"></span> </button>
</div>
@endif
@endsection
<script>

  function search(){
    var form = document.querySelector('#searchForm');
    form.submit()
  }

</script>
