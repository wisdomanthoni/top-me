<div class="container container-fluid  px-2 py-4">
    @if(isset($query))
      <p class="alert alert-success" style="display: none" id="total">
        <strong>{{$total}}</strong> agent{{$total > 1 ? 's':''}} found when searching for <em><strong>{{$query}}</strong></em>
      </p>
    @endif
    <div class="row my-5">
      @if(count($sias)> 0)
        @foreach($sias as $sia)
          @include('includes.sias')
        @endforeach
      @else
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 my-1 my-sm-1 my-md-0 my-lg-0 my-xl-0 d-flex justify-content-center bg-succss">
            <div class="card pb-3 rounded-0 card-shadow" style="width: 19rem;">
                <div class="d-flex justify-content-center px-0 rounded-0">
                  <img class="card-img-top img-fluid mx-0 w-100 rounded-0" src="img/test-img.png" alt="Card image cap">
                </div>
                <div class="card-body">
                  <h5 class=" text-center f-18">No Sias available</h5>
                  <p class="card-text f-14 text-center">Currently, there are no available sias</p>

                  <div class="row ">
                    <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>$0</span></div>
                    <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-money px-2"></span>$0</span></div>
                  </div>
                  <div class="row">
                    <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>0 Donor</span></div>

                  </div>
                  <!-- <div class="row">
                    <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>0 Donor</span></div>
                    <div class="col text-right"><span class="card-text sia-card-icon"></div>
                  </div> -->

                  <!-- <center><a href=""><button  type="button" class="btn btn-sm view-campaign-btn my-3">View Campaigns</button></a></center> -->
                </div>
              </div>
          </div>
      @endif
    </div>
    @if(count($sias) >= 6)
     <div class="d-flex justify-content-end">
        <a href="{{ url('/impact-agents')}}">  <button type="button" class="btn bg-white btn-more font-weight-bold px-4 mt-3">View More  <span class="fas fa-arrow-right ml-2"></span>  </button></a>
      </div>
    @endif
</div>

<script type="text/javascript">
  var p = document.querySelector('#total');
  setTimeout(5000, p.style.display = 'block');
</script>
