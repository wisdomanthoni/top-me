<div class="container container-fluid  px-2 py-4">

    <div class="row my-5">
      @foreach($trend_sias as $sia)
        @include('includes.sias')
      @endforeach
      @if(count($trend_sias)<1)
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4  my-1 my-sm-1 my-md-0 my-lg-0 my-xl-0 d-flex justify-content-center bg-succss">
        <div class="card pb-3 rounded-0 card-shadow" style="width: 19rem;">
            <div class="d-flex justify-content-center px-0 rounded-0">
              <img class="card-img-top img-fluid mx-0 w-100 rounded-0" src="img/test-img.png" alt="Card image cap">
            </div>
            <div class="card-body">
              <h5 class=" text-center f-18">No Trending Sias available</h5>
              <p class="card-text f-14 text-center">Currently, there are no available sias</p>

              <div class="row pl-3">
                <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>$0</span></div>
                <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-money px-2"></span>$0</span></div>
                <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>0 Donor</span></div>

              </div>
            </div>
          </div>
        </div>

      @endif
    </div>
</div>
