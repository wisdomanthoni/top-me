<div class="container container-fluid  px-2 py-4">

    <div class="row my-5">
      {{-- @forelse($sias as $sia)
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4  mt-1 mb-1 mt-sm-1 mb-sm-1 mt-md-0 mb-md-2 mt-lg-0 mb-lg-2 mt-xl-0 mb-xl-2 d-flex justify-content-center justify-content-sm-center justify-content-md-start justify-content-lg-start justify-content-xl-start bg-succss">
        <div class="card pb-3 rounded-0 card-shadow" style="width: 19rem;">
          <div class="d-flex justify-content-center  px-0 bg-danger">
            <img class="card-img-top img-fluid  mx-0 w-100" src="{{$sia->picture != null?$sia->picture: '/img/test-img.png'}}" alt="Card image cap">
          </div>
          <div class="card-body">
            <h5 class=" text-center f-18">{{$sia->name}}</h5>
            <p class="card-text f-14">Lorem ipsum dolor sit amet consetetur dispsicing elt, sed do eiusmod incidiunt ut laborellamco labo....<a href="{{ url('/profile')}}" class="continue-btn" style="text-decoration:none!important;">Continue reading</a></p>

            <div class="row">
              <div class="col text-center"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>300 Funded Projects</span></div>
              <div class="col pr-3"><span class="card-text sia-card-icon"><span class="fas fa-money px-2"></span>$4,000,000 raised</span></div>
            </div>

            <div class="row px-2">
              <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>1200 Donors</span></div>
              <div class="col text-right"><span class="card-text sia-card-icon"></div>
            </div>

            <center><a href="{{ url('/profile')}}"><button  type="button" class="btn btn-sm view-campaign-btn my-3">View Campaigns</button></a></center>
          </div>
        </div>
      </div>
      @empty --}}
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 my-1 my-sm-1 my-md-0 my-lg-0 my-xl-0 d-flex justify-content-center bg-succss">
        <div class="card pb-3 rounded-0 card-shadow" style="width: 19rem;">
          <div class="d-flex justify-content-center px-0 rounded-0">
            <img class="card-img-top img-fluid mx-0 w-100 rounded-0" src="img/test-img.png" alt="Card image cap">
          </div>
          <div class="card-body">
            <h5 class=" text-center f-18">No Trending Sias available</h5>
            <p class="card-text f-14 text-center">Currently, there are no available sias</p>
            <div class="row pl-3">
              <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>$0</span></div>
              <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-money px-2"></span>$0</span></div>
              <div class="col"><span class="card-text sia-card-icon"><span class="fas fa-users px-2"></span>0 Donor</span></div>
            </div>
          </div>
        </div>
      </div>

      {{-- @endforelse --}}
    </div>
</div>
