{{-- {{dd(Auth::user()->notifications)}} --}}
@foreach(Auth::user()->unreadNotifications as $notification)
	@php
		$read = false;
	@endphp
	@if($notification->type == 'App\Notifications\CampaignComments')
		@include('notifications.commented_campaign')
	@endif

	@if($notification->type == 'App\Notifications\CampaignFunded')
		@include('notifications.funded_campaign')
	@endif

	@if($notification->type == 'App\Notifications\CampaignLiked')
		@include('notifications.liked_campaign')
	@endif
@endforeach

@foreach(Auth::user()->readNotifications as $notification)
	@php
		$read = true;
	@endphp
	@if($notification->type == 'App\Notifications\CampaignComments')
		@include('notifications.commented_campaign')
	@endif

	@if($notification->type == 'App\Notifications\CampaignFunded')
		@include('notifications.funded_campaign')
	@endif

	@if($notification->type == 'App\Notifications\CampaignLiked')
		@include('notifications.liked_campaign')
	@endif	
@endforeach

