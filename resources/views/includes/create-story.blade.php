<div class="container  p-sm-0 p-0">
    <div class="d-flex justify-content-center bg-sucss p-sm-0 p-0 p-md-3 p-lg-3 p-xl-3">
        <div class="d-block card-shadow-alt w-100-alt-diff">
          <div class="d-flex justify-content-center py-4 border-bottom">
            <i class="fas fa-plus-circle mt-0 mt-sm-0 mt-md-2 mt-lg-2 mt-xl-2 mb-0 mr-3"></i>
            <p class="text-center my-0 f-18">Create a Story</p>
          </div>
            <div class="container container-fluid p-sm-3 p-3 p-md-3 p-lg-0 p-xl-0 bg-daer">
              <form action="/create-campaign" method="POST" enctype="multipart/form-data" class="mx-sm-1 mx-2 mx-md-0 mx-lg-3 mx-xl-3 px-sm-0 px-0 px-md-2 px-lg-5 px-xl-5">
                @csrf
                {{-- <div class="form-group my-4">
                  <input type="text" name ="category" class="form-control form-control-lg" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Category of Post">
                </div> --}}
                <div class="form-group my-4">
                  <input type="text" name="title" class="form-control  form-control-lg" id="exampleInputPassword1" placeholder="Title" required>
                </div>
                <div class="form-group my-4">
                  <input type="number" name="amount" class="form-control  form-control-lg" id="exampleInputPassword1" placeholder="Amount Needed" required>
                </div>
                <div class="form-group mt-4 mb-0">
                  <textarea class="form-control" name="body" class="form-control form-control-lg" id="exampleFormControlTextarea1" placeholder="Story Details" rows="5" required></textarea>
                </div>
                <div class="d-flex justify-content-center justify-content-sm-center justify-content-md-end justify-content-lg-end justify-content-xl-end mt-3 mt-sm-3 mt-md-2 mt-lg-0 mt-xl-0">
                  
                <div id="fileSelector" class="f-16" style="cursor:pointer">
                  <i class="fas fa-cloud-upload-alt mt-2 mb-0 mr-2"></i>Upload Files

                </div>
                <button id="upload_widget_openers">Open Widget</button>
                <input type="hidden" name="pictures" id="inputPic">
                  

                </div>
                <div id="image" class="text-center text-sm-center text-md-left text-lg-left text-xl-left mt-2 mt-sm-2 mt-md-2 mt-lg-0 mt-xl-0">Maximum of 3 files can be uploaded per campaign</div>
                <div class="d-flex border-bottom" id="imageHolder">
                    </div>

                <div class="d-flex justify-content-center my-4">
                  <button type="submit" class="btn btn-lg btn-purple px-5 mb-5">Create</button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script>
  var fileSelector = document.querySelector('#fileSelector');
  var imageHolder = document.querySelector('#imageHolder')

  fileSelector.addEventListener('click', function(){
    document.querySelector('.cloudinary-button').click();
    var oImg = imageHolder.querySelectorAll('.im');
      for (var i = 0; i < oImg.length; i++) {
        imageHolder.removeChild(oImg[i]);
        
      }
  })
    
  $('#upload_widget_openers').cloudinary_upload_widget(
      { cloud_name: 'djz6ymuuy', upload_preset: 'kt98ybsg', folder: 'test', theme:'purple', max_files:3 },

      function(error, result) {
        if (result) {
          var urlList ="";  
          for (var i = 0; i < result.length; i++) {
            var img = document.createElement('img');
            img.classList ='p-2 im';
            img.src = result[i].url;
            img.style.width = "100px";
            img.style.height = "100px";
            imageHolder.appendChild(img);
            urlList += ", "+result[i].url;
          }
          document.querySelector('#inputPic').value = urlList;
        }
         
      });
    
</script>
