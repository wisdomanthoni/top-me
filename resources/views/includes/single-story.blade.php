
@extends('layouts.app-dashboard')

@section('other_metas')
  <meta name="description" content="{{ $campaign->body, 0, 90 }} ">
  <meta property="og:image"  content="{{ $campaign->photos[0]->url }}">
  <meta property="og:image:secure_url" content="{{ $campaign->photos[0]->url }}">
  <meta property="og:image:width"      content="1200">
  <meta property="og:image:height"     content="1200">
@endsection

@section('content')

@include('chrome.header-dashboard')

    <!-- if aan sia is the ont viewing the single story show the sia dashbpard -->
    {{--@include('includes.sia-dashboard-nav-2')--}}
    <div class="container container-fluid mt-4">
      <a href="{{ url('/dashboard')}}" class="text-dark f-18">
        <i class="fas fa-arrow-left mr-2"></i><span class="f-14">Go back</span>
      </a>
    </div>
    <!-- else show the user dashboard -->
    <div class="d-flex justify-content-center bg-sucess px-1">
      <div class="row mx-0 w-100 mx-5 pt-4">
        <div class="col-lg-7 bg-suss">
          <div class="container container-fluid">

            <div class="container container-fluid">
              <div class="card border rounded-0">
                <div class="">
                  @isset($campaign->photos[0]->url)
                  <img class="card-img-top w-100 h-100" src="{{$campaign->photos[0]->url}}" alt="Card image">
                  @else
                  <img class="card-img-top" src="{{ asset('/img/Topme Logo.png')}}" alt="Card image">
                  @endif
                </div>
                @isset($campaign->photos[1])
                <div class="col-12 col-md-4">
                  <div class="card">
                    <img class="card-img-top" src="{{ $campaign->photos[1]->url}}" alt="Card image">
                  </div>
                </div>
                @endisset
                <div class="card-body">
                  <!-- <h5 class="card-title">Card title</h5> -->
                  <div class="d-flex justify-content-between bg-anger border-bottom py-3">
                    <div class=" bgccess">
                      <p class="f-32 mb-0" style="line-height:32px">{{$campaign->title}}</p>
                    </div>
                    <div class="bg-prary" style="width:30%;" >
                      <div class="d-flex justify-content-between">
                        <div class="d-flex">
                          <i class="fas fa-heart  f-28 fav mr-1 {{'a' .$campaign->id}}" id="" style="cursor: pointer; @auth {{Auth::user()->isFavourite($campaign->id)? 'color: #8b47b0;' : 'color: black'}} @else 'color: black'; @endauth"  onclick="@auth addToFavourites({{$campaign->id}}, this) @else alert('Sign In to add a story to favourite') @endauth"> </i>
                          <span class="f-14 text-secondary ml-0">200</span>
                        </div>
                        <div class="d-flex">
                          <i class="fas fa-share-alt f-28 mr-1" style="cursor: pointer;" @auth data-toggle="modal" data-target="#myModal" @else onclick="alert('Sign in to share')" @endauth></i>
                          <span class="f-14 text-secondary ml-0">40</span>
                        </div>

                        <!-- <p class="f-12">21 days left</p> -->
                      </div>
                    </div>
                  </div>
                  <div class="py-3">
                    <p class="card-text">{{$campaign->body}}</p>
                  </div>
                </div>
              </div>

              <div class="card my-4">
                <form method="POST" enctype="multipart/form-data" action="{{route('comment', $campaign->id)}}">
                  @csrf
                  <div class="card-body border-bottom">
                    <div class="d-flex  pb-3" >
                      @auth
                        <img src="{{ Auth::user()->picture!= null? Auth::user()->picture:  asset('/img/test-img.png')}}" alt="" style="width:50px; height:50px; border-radius:50px" class="img-fluid mr-3">
                      @else
                        <img src="{{ asset('/img/test-img.png')}}" alt="" style="width:50px; height:50px; border-radius:50px" class="img-fluid mr-3">
                      @endauth
                      <input type="text" name="body" value="" class="form-control form-control-lg border-0 " style="outline-style: none!important;" {{Auth::user()==null? 'readonly' : ''}} placeholder="Leave a Comment" required="">
                      
                    </div>
                    <div class="d-flex border-bottom" id="imageHolder">
                    </div>
                    <div class="d-flex justify-content-between">
                      <div class="">
                        <button type="button" {{Auth::user()==null? 'disabled' : ''}} onclick="document.querySelector('#fileUpload').click()" class="btn btn-light-alt rounded my-2 px-5"><span class="f-10"> upload photos</span> </button>
                        <input name="image[]" accept="image/*" onchange="handleImage(this.files)" style="display: none;" type="file" id="fileUpload" multiple>
                      </div>
                      <div class="">
                        @auth
                          <button type="submit" class="btn btn-purple-alt my-2 px-5"> Post</button>
                        @else
                          <button type="button" onclick="alert('You must login to comment')" class="btn btn-purple-alt my-2 px-5"> Post</button>
                        @endauth
                        
                      </div>
                    </div>
                  </div>
                </form>
                @foreach($comments as $comment)
                <div class="card-body border-bottom" id="comment-{{$comment->id}}">
                  
                  <div class="row py-4">
                    <div class="col-2">
                      <!-- <i class="fas fa-user-circle fa-3x mr-3 text-secondary"></i> -->
                      <img src="{{ $comment->user->picture == null? asset('/img/test-img.png') : $comment->user->picture}}" alt="" style="width:50px; height:50px; border-radius:50px" class="img-fluid">
                    </div>
                    <div class="col-10 bg-dangr px-0">
                      <div class="d-flex justify-content-between">
                        <div class=" mt-1">
                          <p class="f-18 mb-0" style="line-height:24px">{{ ucwords($comment->user->name) }}</p>
                          <p class="f-11 text-muted">{{$comment->created_at->diffForHumans()}}</p>
                        </div>
                        <div class="d-flex mt-1 mr-3" style="color:#4F4F4F;">
                          <div class="d-flex mr-3">
                            <i class="fas fa-heart f-20 mr-1" id="" style="cursor: pointer;"> </i>
                            <!-- <span class="f-14 text-secondary ml-0">200</span> -->
                          </div>
                          <div class="d-flex">
                            <i class="fas fa-share-alt f-20 mr-1" style="cursor: pointer;" @auth onclick="shareModal({{$campaign->id}})" @else onclick="alert('Sign in to share')" @endauth></i>
                            <!-- <span class="f-14 text-secondary ml-0">40</span> -->
                          </div>
                        </div>
                      </div>
                      <div class=" pr-3">
                        <div class="d-flex">
                          @foreach($comment->photos as $pic)
                            <img class="p-2" src="{{$pic->url}}" width="200px" height="200px">
                          @endforeach
                        </div>
                        <p>
                          {{$comment->body}}
                        </p>
                      </div>
                    </div>
                  </div>
                  
                </div>
                @endforeach
              </div>
              <div class="card "style="margin-top:10%">
                <div class="card-body">
                  <p class="f-18 mb-0">Other Campaigns by {{$campaign->creator->name}}</p>
                </div>
              </div>
              <div class="row">
              @foreach($otherCampaigns as $camp)
              <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6  mt-1 mb-3 mt-sm-1 mb-sm-3 mt-md-0 mb-md-5 mt-lg-0 mb-lg-5 mt-xl-0 mb-xl-5 d-flex justify-content-center justify-content-sm-center justify-content-md-start justify-content-lg-start justify-content-xl-start bg-succss">
                <div class="card pb-3 rounded-0 card-shadow campaign-card" style="width: 21rem;">
                  <div class="text-center bg-succes px-0 rounded-0">
                    @isset($camp->photos[0]->url)
                    <img class="card-img-top img-fluid clickable-card  rounded-0 mx-0 w-100" style="border-radius:0px!important; cursor: pointer;" src="{{$camp->photos[0]->url}}" alt="{{$camp->title}}" onclick="singleStory({{$camp->created_at->timestamp}})" >
                    @else
                     <img class="card-img-top img-fluid clickable-card  rounded-0 mx-0 w-100" style="border-radius:0px!important; cursor: pointer;" src="{{ asset('/img/Topme Logo.png')}}" alt="Topme.ng" onclick="singleStory({{$camp->id}})" >
                    @endisset
                  </div>
                  <div class="card-body mb-0">
                    <div class="border-bottom mb-3 pb-2">
                      <h5 class="card-title ">{{$camp->title}}</h5>
                      <p class="card-text">{!!strlen($camp->body) > 150 ?substr($camp->body, 0, 150).'...' : $camp->body !!}
                        @if(strlen($camp->body) > 150)
                          <a href="{{route('single-story', $camp->created_at->timestamp)}}" style="color: blue;">view more</a>
                        @endif
                        </p>
                    </div>
                    <div class="border-bottom mb-3 pb-2">
                      <div class="progress ">
                        <div class="progress-bar prog-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                      <div class="row">
                        <div class="col-4 col-md-4">
                          <label>&#8358;0<span class="donors"> raised</span></label>
                        </div>
                        <div class="col-4 col-md-4 text-center">
                          <label><img class= "svg" src="img/user-silhouette.svg">0<span class="donors"> Donors</span></label>
                        </div>
                        <div class="col-4 col-md-4 text-right">
                          <label><span class="donors">Total</span> &#8358;{{number_format($camp->amount, 2, '.', ',')}}</label>
                        </div>
                      </div>
                    </div>
                    <div class="row mb-0">
                      <div class="col-5 col-md-5">
                        <!-- <div class=" d-flex justify-content-end">
                          <button type="button" class="btn bg-transparent btn-more-alt rounded py-0">view more</button>
                        </div> -->
                      </div>
                      <div class="col-7 col-md-7">
                        <div class="d-flex justify-content-between">
                          <i class="fas fa-heart fa2x  {{'a' .$camp->id}}" id="" style="cursor: pointer; @auth {{Auth::user()->isFavourite($camp->id)? 'color: #8b47b0;' : 'color: black'}} @else 'color: black'; @endauth"  onclick="@auth addToFavourites({{$camp->id}}, this) @else alert('Sign In to add a story to favourite') @endauth"></i>
                          <i class="fas fa-share-alt fa-2 " style="cursor: pointer;" @auth onclick="" @else onclick="alert('Sign in to share')" @endauth></i>
                          <!-- <p class="f-12">21 days left</p> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-5 bg- nger">
          <div class="container container-fluid">
            <div class="card">
              <div class="card-body bg-dnger ">
                <div class="d-flex justify-content-between bg-uccess">
                  <div class="">
                    <p class="f-16 mb-0 bg-waning"> <span class="text-purple f-20">&#8358;{{ number_format($campaign->funds->sum('amount')) }}</span> &nbsp;&nbsp; of &nbsp;&nbsp; <span class="text-dark f-18">&#8358;{{ number_format($campaign->amount) }}</span> </p>
                  </div>
                  <div class="pt-1">
                    <p class="f-16 "> <i class="fas fa-user text-purple"></i> {{count($campaign->donors())}} Donors</p>
                  </div>
                </div>
                <div class="progress">
                  <div class="progress-bar prog-bar" role="progressbar" style="width: {{ $campaign->funds->sum('amount') / $campaign->amount * 100 }}%;" aria-valuenow="{{ $campaign->funds->sum('amount') / $campaign->amount * 100 }}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="mt-3">
                  <button type="button" class="btn btn-purple-alt w-100 my-2"  data-toggle="modal" data-target=".bd-example-modal-lg"> Donate</button>
                  <button type="button"@auth data-toggle="modal" data-target="#myModal" @else onclick="alert('Sign in to share')" @endauth class="btn btn-white-alt w-100 mt-2"> Share on social media</button>
                </div>
              </div>
            </div>
            <div class="d-flex justify-content-between my-5">
              <div class="">
                <!-- <i class="fas fa-user-circle fa-4x text-muted"></i> -->
                <img src="{{ $campaign->creator->picture == null? asset('/img/test-img.png') : $campaign->creator->picture}}" alt="" style="width:50px; height:50px; border-radius:50px" class="img-fluid">
              </div>
              <div class="">
                <p class="f-18 mb-0">{{ ucwords($campaign->creator->name) }}</p>
                <p class="f-10">Created on  {{$campaign->created_at->englishMonth .' ' . $campaign->created_at->day. ', '. $campaign->created_at->year. '.'}}</p>
              </div>
              <div class="">
                <button type="button" @auth data-toggle="modal" data-target="#myModal" @else onclick="alert('Sign in to share')" @endauth class="btn btn-white-alt w-100 mt-2 border py-2"> Share on social media</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Choose A Platform to share {{$campaign->title}}</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-12 col-md-4" style="background-color: red;">
          <a href="https://www.facebook.com/sharer/sharer.php?u={{route('single-story', $campaign->slug)}} "target="_blank">
            Share on Facebook
          </a>
        </div>
        <div class="col-12 col-md-4" style="background-color: red;">
          <a href="https://twitter.com/intent/tweet?url={{route('single-story', $campaign->slug)}}" target="_blank">
            Share on Twitter
          </a>
        </div>
        <div class="col-12 col-md-4" style="background-color: red;">
          <a href="https://plus.google.com/share?url={{route('single-story', $campaign->slug)}}"
            target="_blank">
            Share on Google plus
          </a>
        </div>
        </div>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
@include('modals.donate')

@endsection

@section('other_scripts')
<script>
  var imgParent = document.querySelector('#imageHolder');
  function handleImage(files){
    var oImg = imgParent.querySelectorAll('.im');
    for (var i = 0; i < oImg.length; i++) {
      imgParent.removeChild(oImg[i]);
      // oImg[i]
    }
    var img = document.createElement('img');
    for (var i = files.length - 1; i >= 0; i--) {
      if (files[i].type.startsWith('image/')) {
        var img = document.createElement('img');
        img.style.width = "100px";
        img.style.height = "100px";
        img.classList = 'im p-2';
        img.file = files[i];

        var reader = new FileReader();
        reader.onload = (function(a){
          return function(e){a.src = e.target.result}
        })(img)
        reader.readAsDataURL(files[i]);
        imgParent.appendChild(img);
      }
    }
  }
</script> 
@endsection
