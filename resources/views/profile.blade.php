@extends('layouts.app')

@section('content')
@php
  $name = explode(' ', $user->name);
  // dd($user);
@endphp
<div id="profile-banner">

    <div class="container">
      <div class="row pr-3">
        <div class="col-md-8 my-2 my-sm-2 my-md-3 my-lg-5">
          <p class="profile-name text-white text-center tect-md-left text-lg-left">{{ucwords($user->name)}}</p>
          <p class="profile-occupation text-white mb-0 text-center tect-md-left text-lg-left">{{$user->occupation!=null? ucwords($user->occupation):'None added'}}</>
          {{-- <p class="profile-social-account text-white">IG: @katehenshaw</p> --}}
        </div>

        <div class="col-md-4 my-2 my-sm-2 my-md-3 my-lg-5">
          <button type="button" class="btn bg-white font-weight-bold py-3 my-0 my-sm-0 my-md-3 my-lg-5 px-4 btn-hover-purple f-24">DONATE TO {{strtoupper($name[0])}}'S PROJECTS</button>
        </div>
      </div>
    </div>

</div>

<div class="container" id="profile-about">
  <div class="row">
      <div class="col-md-4 my-5">
        <img class="img-fluid" src="{{$user->picture != null? $user->picture : '/img/test-img.png'}}" style="">
      </div>

      <div class="col-md-8 my-5">
        <div class="container">
          <p class="f-36 font-weight-bold text-purple">Bio:</p>
          <p>{{$user->objective}}</p>
          <!-- <p class="card-text">qwertyuioiuytrewqwertyuioiuytrewer tyuioiuytrewwertyuioiuytrewwertyuioiuy trewertyui uytrewertyuasdfghjk jhgfdsasdfghj kjhgfds sdfgh</p> -->
        </div>

        <div class="row px-3 my-4 pt-2" id="text-more-info">
          <div class="col-md-2"><span class="text-more-info">Age</span> : {{$user->age}} years</div>
          <div class="col-md-4"><span class="text-more-info">Work</span> : {{$user->occupation}}</div>
          <div class="col-md-3"><span class="text-more-info">Location</span> : {{$user->location}}</div>
          <div class="col-md-3"><span class="text-more-info">Family</span> : {{$user->marital_status}}</div>
        </div>
      </div>
  </div>
</div>



<section id="Total" class="mt-0">

  <div class="bg-light py-0 my-0 bg-dager">
    <div class="container container-fluid my-1 my-sm-1 mb-md-5 mt-md-0 mt-lg-0 mb-lg-5 py-3">
      <div class="row">
        <div class="col-6 col-md-4 text-sm-center text-center text-md-left text-lg-left">
          <p class="text-purple text-rambla f-18 my-0 font-weight-bold ml-2" style="line-height:22px;">TOTAL CAMPAIGNS:</p>
          <p class="f-64 text-roboto font-weight-bld my-0" style="line-height:75px;">{{ $count_campaigns }}</p>
        </div>
        <div class="col-6 col-md-4 text-center">
          <p class="text-purple text-rambla f-18 my-0 font-weight-bold" style="line-height:22px;">TOTAL NUMBER OF SIAS:</p>
          <p class="f-64 text-roboto font-weight-old my-0" style="line-height:75px;">{{$count_sia}}</p>
        </div>
        <div class="col-12 col-md-4 text-md-right text-lg-right text-sm-center text-center mt-3 mt-sm-3 mt-md-0 mt-lg-0">
          <p class="text-purple text-rambla text-left text-sm-left text-md-center text-lg-center f-18 my-0 font-weight-bold ml-50-alt" style="line-height:22px; margin-left:30%;">TOTAL DONORS:</p>
          <p class="f-64 text-roboto font-weight-bod my-0" style="line-height:75px;">{{$total_donors}}</p>
        </div>
      </div>
    </div>
  </div>



</section>




  <div class="container my-5 pt-5" id="Campaigns-created">

    <div class="d-block d-md-flex d-lg-flex justify-content-center justify-content-md-between justify-content-lg-between">
      <div class="text-center "><p class="f-24 font-weiht-bold">Campaigns created</p></div>
      <div class="d-flex justify-content-center"><button type="button" class="btn bg-white px-5 border">Talk to {{ucwords($name[0])}}?</button></div>
    </div>


<!-- second card deck -->
  @include('includes.sias-campaigns')
<div class="d-flex justify-content-center justify-content-sm-center justify-content-md-end justify-content-lg-end justify-content-xl-end bg-succss">{!! $campaigns->render() !!}</div>
</div>  <!-- close card Campaigns -->
</div> <!-- close card container -->
@php
  $sias = $sias->take(4)
@endphp
<div class="container my-5 pt-2" id="people-also-viewed">
  <p class="f-24 font-weight-bold text-center text-sm-center text-md-left text-lg-left">People also viewed</p>
  <div class=" ">
    <div class="row">
      @foreach($sias as $sia)
      <div class="col-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 mb-3 pr-0 d-flex justify-content-center justify-content-sm-center justify-content-md-start justify-content-lg-start justify-content-xl-start">
        <div class="card rounded-0" style="width: 8.5rem;">
          <img class="card-img rounded-0" src="{{$sia->picture!=null?$sia->picture:'/img/test-img.png'}}" alt="Card image cap">
          <div class="card-body py-0 pr-0 pl-1">
            <p class="f-16 font-weight-bold">{{ucwords($sia->name)}}</p>

           </div>
        </div>
      </div>
      @endforeach

    </div>
  </div>
</div>

<div class="container my-0 my-md-5 my-lg-5 pt-0 pt-md-5 pt-lg-5" id="have-a-project">

  <div class="text-center my-0 my-md-5 my-lg-5 pt-0 pt-md-5 pt-lg-5">
    <p class="have-a-project-text">Have a Project you’d like {{ucwords($name[0])}} to Champion?</p>
    <p class="have-a-project-text2">Drop a message</p>
  </div>

</div>


@endsection
