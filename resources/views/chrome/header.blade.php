<nav class="navbar navbar-expand-lg navbar-light bg-light-alt px-0 px-sm-1 px-md-3 px-lg-5 px-xl-5 pt-1 pb-1 pl-2 pr-3">
  <a class="navbar-brand" href="{{ url('/') }}"><img class="img-fluid" src="{{ asset('/img/Topme Logo.png')}}" style="height:60px; width:150px;"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item mx-2">
        <a class="nav-link f-18 font-weight-bold" href="{{url('/about')}}">About <span class="sr-only">(current)</span></a>
      </li>
      {{-- <li class="nav-item mx-2">
        <a class="nav-link f-18 font-weight-bold" href="#">How it works</a>
      </li> --}}
      <li class="nav-item mx-2">
        <a class="nav-link f-18 font-weight-bold" href="#">Blog</a>
      </li>
      @auth
      <li class="nav-item mx-2">
        <form method="POST" action="{{route('logout')}}">@csrf <button style="background-color: transparent; border: none; cursor: pointer; outline: none;" class="nav-link f-18 font-weight-bold">Logout</button></form>
      </li>
      @else
      <li class="nav-item mx-2">
        <a class="nav-link f-18 font-weight-bold " href="{{ route('login') }}">Login</a>
      </li>
      <li class="nav-item mx-2 px-0 px-sm-0 px-md-3 px-lg-3 px-xl-3 pt-2">
        <a class="nav-link f-18 font-weight-normal purple-hover rounded btn py-2 py-sm-2 py-md-0 py-lg-0 py-xl-0 px-5 text-purple" href="{{ route('register') }}"  style="border:2px solid #490F83">Sign Up</a>
      </li>
      @endauth

    </ul>

    <!-- <form class="form-inline my-0 my-lg-0 mx-2 ">
        <input class="form-control search-input bg-transparent" type="text" placeholder="Search.." name="search" size="15">
        <i class="fas fa-search text-right"style="margin-top:-20%;margin-left: 75%; "></i>

     </form> -->
  </div>
</nav>
