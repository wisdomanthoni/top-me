<div class="card card-shadow" style="{{$read ? 'background-color: #ededed;':''}}">
  <div class="d-flex  border-bottom py-3 my-0">
    <i class="fas fa-user-circle mx-4 fa-2x text-secondary"></i>
    <p class="my-0">{{ \App\User::find($notification->data['user_id'])->id == Auth::user()->id? 'You': ucwords(\App\User::find($notification->data['user_id'])->name)  }} commented on your campaign:<strong><a style="color: black;" href="/campaigns/{{ \App\Campaign::find($notification->data['campaign_id'])->slug }}#comment-{{ $notification->data['comment_id'] }}">{{ \App\Campaign::find($notification->data['campaign_id'])->title }}</a></strong> </p>
  </div>
</div>