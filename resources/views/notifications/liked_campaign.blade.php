<div class="card card-shadow" style="{{$read ? 'background-color: #ededed;':''}}">
  <div class="d-flex  border-bottom py-3 my-0">
    <i class="fas fa-user-circle mx-4 fa-2x text-secondary"></i>
    <p class="my-0">{{ \App\User::find($notification->data['user_id'])->id == Auth::user()->id? 'You': ucwords(\App\User::find($notification->data['user_id'])->name)  }} liked your campaign: <strong>{{ \App\Campaign::find($notification->data['campaign_id'])->title }}</strong> </p>
  </div>
</div>