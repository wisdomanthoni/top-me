@extends('layouts.app')
@section('content')
  <div class="container center ">
        <div class="box mt-5">
            <div class="container d-block">
            <p class="level">3/5</p><br>
            <p class="up">Verify User As SIA
            <hr class="hr">
          </p>
            <form method="POST" action="/register-as-sia">
              @csrf
              <input type="hidden" name="step" value="3">
              <input type="hidden" name="email" value="{{$user->email}}">
              <div class="form-group ">
                <input class="form-control" name="bvn" type="text" placeholder="BVN Number" required>
              </div>
              <div class="form-group ">
                <input class="form-control" name="phone" type="text" placeholder="Phone Number" required>
              </div>
                
              <button type="submit" class="btn Next next form-control">Next</button> <br>           
              <button type="button" class="btn Cancel form-control"><a class="cancel " href="/" >Cancel</a></button>  <br> 
            </form>         
            <p class="Remind"> Have an account? <a class="log" href="">Log In </a></p>            
          </div>
      </div>
      </div>

@endsection
