@php
  $user = session()->get('sia', null);
  $accs =App\LinkedSocialAccount::where('sia_registration_id', $user->id)->get(); 
  // dd(\App\LinkedSocialAccount::all());
  // dd($accs);
@endphp





@extends('layouts.app')
@section('content')

  <div class="container center ">
        <div class="box my-5">
            <div class="container d-block">
            <p class="level">4/5</p><br>
            <p class="up">Verify User As SIA
            <hr class="hr">
          </p>
          
          @if (count($accs) == 0)
            <p class="f-12">Please connect to your twitter handle for verification</p>
            <a href="{{ url('/sia-verify-login/twitter')}}" class="btn f-12 w-100 text-white mb-2" style="background-color:#2F80ED">
              <i class="fab fa-twitter text-white mr-2"></i>Connect to Twitter
            </a>
          @endif
            
          @if (count($accs) > 0)
             @foreach($accs as $a)
                @if($a->provider_name == 'twitter')
                       @php
                         $asset = json_decode($a->assets)
                       @endphp
                      <div class="d-block justify-content-center">
                         <img src="{{$asset->image_url}}" class="img-fluid img-thumbnail h-50 w-50">
                         <br>
                         <span><i>{{$asset->username}}</i></span>
                      </div>
                       <div class="d-flex justify-content-center">
                        <form action="/register-as-sia" method="POST">
                          @csrf
                          <input type="hidden" name="step" value="4">
                          <button type="submit" class="btn f-12 w-50 text-white mt-5 mb-2" style="background-color:#8A33E1">
                            Verify<i class="fas fa-arrow-right text-white ml-2"></i>
                          </button>
                        </form>  
                       </div>
                @endif
             @endforeach
          @endif  
             
             <br>
             <br>
            <button type="button" class="btn Cancel form-control">Back</button>  <br>

          </div>
      </div>
      </div>
@endsection
