<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiaRegistration extends Model
{
    public function accounts(){
       return $this->hasMany('App\LinkedSocialAccount');
    }
}
