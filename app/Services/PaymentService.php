<?php

namespace App\Http\Services;

use App\Http\Controllers\Controller;

// use App\Http\Requests;
use App\Payment;
use App\QuizUnit;
use Illuminate\Http\Request;
use Paystack;

class PaymentService extends Controller
{

    public function index()
    {
        $data['pageTitle']     = "Subscriptions";
        $data['subscriptions'] = Payment::where('status', 'success')->where('user_id', auth()->user()->id)->orderBy('transaction_date', 'DESC')->get();
        return view('users.subscription', $data);
    }

    public function pricing()
    {
        $data['pageTitle'] = "Pricing";
        return view('users.pricing', $data);
    }

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway(Request $request)
    {
        $payment            = new Payment;
        $payment->email     = $request->email;
        $payment->amount    = $request->amount;
        $payment->reference = $request->reference;
        $payment->units     = $request->units;
        $payment->bundle    = $request->bundle ? $request->bundle : '';
        $payment->user_id   = auth()->user()->id;
        if ($payment->save()) {
            return Paystack::getAuthorizationUrl()->redirectNow();
        }

        return redirect()->back()->with('error', "Failed to initialize payment");
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {

        $paymentDetails = Paystack::getPaymentData();

        // return $paymentDetails['data'];

        // $result = dd($paymentDetails);

        if ($paymentDetails['status']) {
            $data = $paymentDetails['data'];

            $payment = Payment::where('reference', $data['reference'])->first();

            $payment->status           = $data['status'];
            $payment->reference        = $data['reference'];
            $payment->transaction_date = $data['transaction_date'];
            $payment->fees             = $data['fees'];
            $payment->save();

            $checkUnits = QuizUnit::where('reference', $data['reference'])->count();
            if ($checkUnits == 0) {
                $quizUnit            = new QuizUnit;
                $quizUnit->user_id   = $payment->user_id;
                $quizUnit->units     = $payment->units;
                $quizUnit->reference = $payment->reference;
                $quizUnit->type      = "Premium";
                $quizUnit->save();
            }
        }

        return redirect('quiz-zone')->with('message', 'Your payment was successful');
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }
}
