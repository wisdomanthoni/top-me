<?php

namespace App\Repositories;

use App\User;
use Auth;
use App\UserRole;
use App\Role;
use App\SiaRegistration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerification;
use GuzzleHttp\Client;
use App\Repositories\PhotoRepository;
use Carbon\Carbon;

/**
 * 
 */
class UserRepository
{
	protected $photos;
	function __construct()
	{
		$this->photos = new PhotoRepository;
	}

	public function getUser($id){
		$user = User::find($id); 	//get the user with the given Id
		if ($user) {				//check if user exists
			return $user;
		}else{
			return null;
		}
	}
	public function getAllSia(){
		$sias = [];
		foreach (User::all() as $user) {
			foreach ($user->roles as $role) {
				if($role->role == 'SIA'){
					array_push($sias, $user);
				}
			}
		}
		$sias = collect($sias);
		return $sias;
	}

	public function update(Request $request){
		// dd($request->marital);
		$user = Auth::user();
		$user->name = $request->fname.' '.$request->lname;
		// if($request->hasFile('image')){$user->picture = $this->photos->upload($request->file('image'));}
		$user->picture = 'https'. substr($request->picture, 4);
		$user->objective = $request->bio;
		$user->location = $request->location;
		$user->occupation = $request->occupation;
		$user->marital_status = $request->marital;
		$user->save();
		return $user;
	}
	public function makeSIA($id){
		$user = User::find($id);
		if($user != null){		//checks for user
			if($this->setRole($user, 'SIA')){		//tries setting the role
				// $user->picture
				return true;
			}else{
				return false;				//if setting role fails
			}

		}else{
			return false;					//user not found or has already been assigned this role
		}
	}
	public function setRole($user, $val){
		$role = Role::where('role', $val)->first();		//try getting role as an instance of Role
		if ($role == null) {
			return false;
		}
		// check if the user already has this role
		$userRole = UserRole::where('user_id',$user->id)->where('role_id',$role->id)->first();
		if ($userRole != null) {
			return false;		//user already has the assigned role
		}
		UserRole::create([
			'user_id'	=>	$user->id,
			'role_id'	=>	$role->id
		]);
		return true;
	}

	public function registerSia(Request $request, $step){
		if($step == 1 ){
			$sia = new SiaRegistration;
			$sia->name = $request->name;
			$sia->email = $request->email;
			$sia->password = Hash::make($request->password);
			$sia->verify_token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 30);
			$sia->save();
			// Mail::to($sia->email)->send(new EmailVerification($sia));
			return ['step'=>++$step, 'sia'=>$sia];


			


		}
		if ($step == 3) {
			$sia = SiaRegistration::where('email', $request->email)->first();
			if($sia == null){return ['step'=>1, 'sia'=>$sia];}
			$client = new Client(['headers' => ['Authorization' => 'Bearer '.env('PAYSTACK_API_KEY')]]);
			$res = $client->request('GET', "https://api.paystack.co/bank/resolve_bvn/".$request->bvn);
			$response = json_decode($res->getBody()->getContents());

			if ($response->data->mobile == $request->phone) {
				$sia->bvn_verified = true;
				$sia->save();
				return ['step'=>++$step, 'sia'=>$sia];
			}else{
				return ['step'=>$step, 'sia'=>$sia, 'error'=>'Phone number supplied does not match that used in bvn registration'];
			}
		}
		if ($step == 4) {
			$sia = $user = session()->get('sia', null);
			$sia->twitter_verified = true;
			//for now, let us automatically verify the user and make him an sia
			$sia->resolved = true;
			$sia->save();

			$user = $this->registerUser($sia);
			return ['user'=>$user, 'sia'=>$sia];
		}

		
	}

	public function verifySiaEmail($token, $id){
		$sia = SiaRegistration::where('id', $id)->where('verify_token', $token)->first();
		if ($sia !=null) {
			$sia->email_verified = true;
			$sia->save();
			return $sia;
		}else{
			return false;
		}
	}

	public function hasStartedSiaRegistration($request){
		$sia = SiaRegistration::where('email', $request->email)->first();
		// dd($sia);
		if ($sia == null) {
			return false;
		}
		if($sia->resolved){
			$step = 6;
			$user = User::where('email', $sia->email)->first();
			return ['step'=>$step, 'sia'=>$sia, 'user'=>$user];
		}
		if($sia->twitter_verified){
			$step = 5;
			return ['step'=>$step, 'sia'=>$sia];
		}elseif ($sia->bvn_verified) {
			$step = 4;
			return ['step'=>$step, 'sia'=>$sia];
		}elseif ($sia->email_verified) {
			$step = 3;
			return ['step'=>$step, 'sia'=>$sia];
		}else{
			return ['step'=>2, 'sia'=>$sia];
		}

	}
	public function registerUser($data){
		$user = User::where('email', $data->email)->first();
		if($user == null){
			$name = explode(' ', strtolower($data->name));
        	$name = implode('-', $name);
        	$username = $name . mt_rand(1, 99999);
			$user =  User::create([
	            'name' => $data->name,
	            'email' => $data->email,
	            'password' => $data->password,
	            'username' =>$username
	        ]);
	    }    
	    //($data);
        $this->makeSIA($user->id);
        return $user;
	}

	public function userBecomeSia($user){
		$sia = new SiaRegistration;
		$sia->name = $user->name;
		$sia->email = $user->email;
		$sia->password = Hash::make($user->password);
		$sia->verify_token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 16);
		$sia->save();

		Mail::to($sia->email)->send(new EmailVerification($sia));
	}

	public function checkRegistrationStatus($user){
		// dd($user);
		$reg = SiaRegistration::where('email', $user->email)->first();
		if ($reg == null) {
			return false;
		}else{
			return true;
		}
	}

	public function isSIA(User $user){
		foreach ($user->roles as $role) {
			if ($role->role == 'SIA') {
				return true;
			}
		}
		return false;
	}

	public function getNewSias(){
		$sias = $this->getAllSia()->where('created_at', '>', Carbon::now()->subDays(2));
		// $sias->order('created_at');
		return $sias;
	}

	public function searchSia($query){
		$users = User::where('name',  'LIKE', '%' . $query . '%')->get();
		$sias = [];
		foreach ($users as $user) {
			foreach ($user->roles as $role) {
				if ($role->role == 'SIA' ) {
					array_push($sias, $user);
				}
			}
		}
		$sias = collect($sias);
		return $sias;
	}

}