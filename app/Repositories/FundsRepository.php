<?php

namespace App\Repositories;


use App\Fund;

/**
 * 
 */
class FundsRepository
{
	
	function __construct()
	{
		# code...
	}

	public function allDonors(){
		$funds = Fund::all();
		$donors = $funds->unique('user_id');
		return $donors;
	}
}