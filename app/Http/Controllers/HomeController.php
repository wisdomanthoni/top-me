<?php

namespace App\Http\Controllers;

use Auth;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Repositories\CampaignRepository;
use App\Repositories\FundsRepository;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $users;
    protected $funds;
    protected $campaigns;

    public function __construct()
    {
        $this->users = new UserRepository;
        $this->campaigns = new CampaignRepository;
        $this->funds = new FundsRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (isset(Auth::user()->name)) {
        //     return redirect('/dashboard');
        // }
        $allSia =$this->users->getAllSia();
        $newSias = $this->users->getNewSias();
        $trend_sias = [];       //TODO logic for trending sias
        $hfunded_sias = [];     //TODO logic for highly funded sias
        // dd($newSias); 
        $sias = $allSia->take(6);
        $count_sia = count($allSia);
        $allCampaigns = $this->campaigns->getAll();
        $total_donors = count($this->funds->allDonors());
        return view('index', ['sias' => $sias, 'campaigns' =>$allCampaigns, 'count_sia'=>$count_sia, 'total_donors'=> $total_donors, 'newSias'=>$newSias, 'trend_sias'=>$trend_sias, 'hfunded_sias'=>$hfunded_sias]);
    }
    public function home()
    {
        if (isset(Auth::user()->name)) {
            return redirect('/dashboard');
        }
        $total_donors = count($this->funds->allDonors());
        $allCampaigns = $this->campaigns->getAll();
        $sias = $this->users->getAllSia();
        return redirect('/'); 
    }
}
