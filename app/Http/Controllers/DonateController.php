<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Paystack;
use App\Http\Services\PaymentService;


class DonateController extends Controller
{
    public function donate(Request $request)
    {
         // dd($request->all());

      // $this->validate($request,[
      //        'address' => 'required',
      //    ]);

         // $user = User::find(Auth::id());
         // $user->address = $request->address;
         // $user->save();

        if($request->status == 'paypal'){  

              $this->paypal($request);
              
              return redirect()->back()->with('info', 'Paypal coming soon');
        }else{
              $this->paystack($request);

             return Paystack::getAuthorizationUrl()->redirectNow();
        }
    }

    public function handleGatewayCallback()
    {

        $paymentDetails = Paystack::getPaymentData();

        if ($paymentDetails['status']) {
            $data = $paymentDetails['data'];

            // $payment = Payment::where('reference', $data['reference'])->first();

            // $payment->status           = $data['status'];
            // $payment->reference        = $data['reference'];
            // $payment->transaction_date = $data['transaction_date'];
            // $payment->fees             = $data['fees'];
            // $payment->save();

            // $checkUnits = QuizUnit::where('reference', $data['reference'])->count();
            // if ($checkUnits == 0) {
            //     $quizUnit            = new QuizUnit;
            //     $quizUnit->user_id   = $payment->user_id;
            //     $quizUnit->units     = $payment->units;
            //     $quizUnit->reference = $payment->reference;
            //     $quizUnit->type      = "Premium";
            //     $quizUnit->save();
            }

        return redirect()->back()->with('success', 'Your payment was successful');
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }

    protected function paypal(Request $request)
    {

    }

    protected function paystack(Request $request)
    {

    }
}
