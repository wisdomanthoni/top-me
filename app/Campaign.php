<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Campaign extends Model
{
	protected $fillable =['author', 'title', 'body', 'amount', 'slug'];

    public function creator(){
    	return $this->belongsTo('App\User', 'author');
    }
    public function photos(){
    	return $this->belongsToMany('App\Photo','campaign_photos');
    }
    public function funds(){
    	return $this->hasMany('App\Fund');
    }

    public function amount_raised(){
        $amount = $this->funds->sum('amount');
        return $amount;
    }

    public function donors(){
        $ufunds = $this->funds->unique('user_id'); 
        $donors = []; 
        foreach ($ufunds as $fund) {
            $user = User::find($fund->user_id);
            array_push($donors, $user);
        }
        $donors = collect($donors);
        return $donors; 
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    
}
