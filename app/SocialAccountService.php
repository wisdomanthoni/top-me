<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialAccountService
{
    public function findOrCreate(ProviderUser $providerUser, $provider, $asset)
    {
        $account = LinkedSocialAccount::where('provider_name', $provider)
                   ->where('provider_id', $providerUser->getId())
                   ->first();

        if ($account) {
            return $account->sia;
        } else {

        $user = session()->get('sia', null);

        // dd($user);

        // if (! $user) {
        //     $user = User::create([  
        //         'email' => $providerUser->getEmail(),
        //         'name'  => $providerUser->getName(),
        //     ]);
        // }

        $user->accounts()->create([
            'provider_id'   => $providerUser->getId(),
            'provider_name' => $provider,
            'assets'        => $asset,
        ]);
       
         //dd($set);
          return $user;

        }
    }
}