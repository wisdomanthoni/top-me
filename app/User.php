<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','age','location','gender','objective','picture','occupation', 'marital_status','username'
    ];
    public function roles(){
        return $this->belongsToMany('App\Role', 'user_roles');
    }
    public function campaigns(){
        return $this->hasMany('App\Campaign', 'author');
    }

    public function accounts(){
       return $this->hasMany('App\LinkedSocialAccount');
    }

    public function amountRaised(){
        $amount = 0;
        foreach ($this->campaigns as $campaign) {
            $amount = $amount + $campaign->amount_raised();
        }
        return $amount;
    }

    public function donors(){
        $donors = [];
        foreach ($this->campaigns as $campaign) {
            if ($campaign->donors()->isNotEmpty()) {
                array_push($donors, $campaign->donors()->all());  
            }
            
        }
        $donors = collect($donors);
        $donors = $donors->flatten();
        return $donors;
    }

    public function favourites(){
        return $this->hasMany('App\Favourite');
    }

    public function isFavourite($campaign){
        foreach ($this->favourites as $fav) {
            if ($fav->campaign_id == $campaign) {
                return true;
            }
        }

        return false;
    }

    public function comments(){
        return $this->hasMany('App\Comments');
    }

    public function fundedProjects(){
        $projects = [];
        foreach ($this->campaigns as $campaign) {
            if ($campaign->funds->count() != 0) {
                array_push($projects, $campaign);
            }
        }
        return collect($projects);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
